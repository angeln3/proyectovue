var gulp = require('gulp');
var jade = require('gulp-jade');
//var pug = require('gulp-pug');
var sass = require('gulp-sass');
//desisntalar gulp-serve
var webserver = require('gulp-server-livereload');
// desisnatlar run-sequence
var runSequence = require('gulp4-run-sequence');
 
gulp.task('webserver', function(done) {
    return gulp.src('./www')
      .pipe(webserver({ 
        host: 'localhost',
        port: 5600,
		path: './www',
		directoryListing: false,
        open: true,
        defaultFile: 'index.html',
      }));
  });


gulp.task('sass',function(done){
    return gulp.src('./src/sass/*.sass')
        .pipe(sass({
            uotputStyle: 'compact'
        }))
        .pipe(gulp.dest('./www/css'))
}); 

gulp.task('jade',function(done){
    return gulp.src('./src/jade/*.jade')
        .pipe(jade({
            pretty:true
        }))
        .pipe(gulp.dest('./www/'))
}); 

//gulp.task('pug',function(){
//    return gulp.src('.pug/*.pug')
//        .pipe(pug())
//        .pipe(gulp.dest('./www/'))
//}); 

gulp.task('watch',function(done){
    gulp.watch('./src/sass/**/*.sass', gulp.series('sass'));
    gulp.watch('./src/jade/**/*.jade', gulp.series('jade'));

});

gulp.task('default',function(){
	return runSequence('jade', 'sass', 'webserver', ['watch'])

});