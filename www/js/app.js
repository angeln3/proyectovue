const componentes =['tablaBaners.js','menuConfigBanners.js','infoLeer.js','tabs.js'];
for (i=0; i<componentes.length; i++){
    
    $("head").append('<script type="text/javascript" src="./js/componentes/'+componentes[i]+'"></script>');
}  
const app = new Vue({
    el: '#app',
    data:{
        //accesos directos
        titulosAD:['Posición','Descripcion'],
        AcceosDirectos:[
            {posicion:1, descripcion:'Todas las Categorías'},
            {posicion:2, descripcion:'hotsale'},
            {posicion:3, descripcion:'Celulares y Tecnología'},
            {posicion:4, descripcion:'Samsumg'},
            {posicion:5, descripcion:'Celulares y Tecnología'}

        ],
        //productos top
        titulosPT: ['Producto','SKU','Stock'],
        ProductosTop:[
            {producto: 'Xbox', sku:'4532', stock:'10'},
            {producto: 'iPhone', sku:'4565', stock:'200'},
            {producto: 'Samsung', sku:'4565', stock:'200'},
            {producto: 'Bálon', sku:'4565', stock:'200'},
            {producto: 'Audifonos', sku:'4565', stock:'200'},
            {producto: 'Sofá', sku:'4565', stock:'200'},
            {producto: 'Laptop', sku:'4565', stock:'200'},
        ],
        //emarsys
        Emarsys:[
            {logica:'Carrusel Productos'},
            {logica:'Carrusel Ofertas del dia'},
            {logica:'Carrusel Celulares'},
            {logica:'Opción Todos'},
            {logica:'Opción Categoría'},
            
        ]

    },
    
})