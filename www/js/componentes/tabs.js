Vue.component('tabs',{
    template: //html
    `
<div class="cajaModulosNew">
  <div class="menuModulos">
    <div class="itemsModulos" v-for="it of itemInfo" :id="it.iditem">{{it.titulo}}</div>
  </div>
  <div v-for="itm of itemInfo" :class="itm.class" :id="itm.id">
    <div class="imgContent" v-if="itm.item==='Banners'"><img class="imgModuloRes1" src="img/index/celular-img.svg"/>
      <div class="txtContent">
        <p class="tituloContent">Banners</p>
        <p class="SubtituloContent">Especificaciones de Banners</p>
      </div>
    </div>
    <div class="imgContent" v-else-if="itm.item==='Accesos Directos'"> 
      <div class="imgContent2"><img class="imgModuloRes2" src="img/index/celular-img.svg"/>
        <div class="textImagen"> </div>
      </div>
    </div>
    <div class="imgContent" v-else-if="itm.item==='Productos top'">
      <div class="imgContent2"><img class="imgModuloRes2" src="img/index/celular-img.svg"/>
        <div class="textImagen">        </div>
      </div>
    </div>
    <div class="imgContent" v-else-if="itm.item==='Tiendas/Marcas'"><img class="imgModuloRes" src="img/index/celular-img.svg"/></div>
    <div class="imgContent" v-else-if="itm.item==='Emarsys'"><img class="imgModuloRes5" src="img/index/celular-img.svg"/></div>
    <div class="descripContent">
      <div class="textoContent"> 
        <p class="tituloContent">{{itm.titulo}}</p>
        <p class="SubtituloContent">{{itm.espesificacion}}
          <ul class="ulModulo" v-if="itm.item==='Banners'">
            <li v-for="info of infoTabs">{{info.banners}}</li>
          </ul>
          <ul class="ulModulo" v-if="itm.item==='Accesos Directos'">
            <li v-for="info of infoTabs">{{info.accesos}}</li>
          </ul>
          <ul class="ulModulo" v-if="itm.item==='Productos top'">
            <li v-for="info of infoTabs">{{info.productostop}}</li>
          </ul>
          <ul class="ulModulo" v-if="itm.item==='Tiendas/Marcas'">
            <li v-for="info of infoTabs">{{info.tiendasmarcas}}</li>
          </ul>
        </p>
        <div class="comenzar"><a :href="itm.href">
            <p class="comenzarT">COMENZAR </p><img src="img/index/ios-arrow-round-forward.svg"/></a></div>
      </div>
    </div>
  </div>
</div>
    `,
    data(){
        return{
            itemInfo:[
                {href:'listaBanners.html',iditem:'ITMcontent1',classimg:'imgModuloRes1',class:'moduloContent',id:'content1',item:'Banners',titulo:'Banners',espesificacion:'Especificaciones de Banners'},
                {href:'accesosDirectos.html',iditem:'ITMcontent2',classimg:'imgModuloRes2',class:'moduloContent2',id:'content2',item:'Accesos Directos',titulo:'Accesos directos',espesificacion:'Especificaciones de los accesos directos'},
                {href:'productosTop.html',iditem:'ITMcontent3',classimg:'imgModuloRes2',class:'moduloContent3',id:'content3',item:'Productos top',titulo:'Productos top',espesificacion:'Los productos top son las mejores ofertas'},
                {href:'bannersTiendasMarcas.html',iditem:'ITMcontent4',classimg:'imgModuloRes',class:'moduloContent4',id:'content4',item:'Tiendas/Marcas',titulo:'Tiendas / Marcas',espesificacion:'Estos banners te apoyarán a posicionar tiendas/marcas'},
                {href:'emarsys.html',iditem:'ITMcontent5',classimg:'imgModuloRes5',class:'moduloContent5',id:'content5',item:'Emarsys',titulo:'Emarsys',espesificacion:'Podras cambiar las lógicas de los productos, como ofertas del día, celulares, por categoría, personalizados de acuerdo a tu estrategia.'}
            ],
            infoTabs:[
                {banners:'Tamaño: 800 x 476px',accesos:'Formato: png',productostop:'Solo so visibles 10 productos',tiendasmarcas:'Tamaño: 480 x 240px',emarsys:''},
                {banners:'Formato: jpg',accesos:'Peso máximo 20Kb',productostop:'Se podrán cargar máximo 40',tiendasmarcas:'Peso máximo 30Kb',emarsys:''},
                {banners:'Peso máximo: 80Kb',accesos:'Tamaño: 200px x 200px ',productostop:'Te recomendamos revisar su stock',tiendasmarcas:'Maximo son 4 banners',emarsys:''},
                {banners:'Resolución de 72ppp',accesos:'Fondo: Transparente',tiendasmarcas:'Resolución de 72ppp',emarsys:''}
            ]
        }
    }
});