$(document).ready(function(){
    
    $("#canalesVenta").click(function(){
        $(".subMenu").slideToggle("slow");
    });

    /*efecto toggle en el menu*/
    var action = 1;

    $("#menuAmb,#t1,#btnEditar,#GEguardar").on("click", toggle);

    function toggle() {
        
       
        if ( action == 1 ) {
            $("p.txt").css("display","none");
             /*agregar una clase y removerla*/
            $("ul.subMenu li").css({"padding-left" : "0px", "text-align": "center", 'background': '#F4F7F9'});
            $("#contenedorMenu").css("width","5%");
            $("#contenedor").css("width","95%");  
            $("#menuAmb").css("display","none");
            $("#seller").fadeOut();
            $(".cajat1").css("margin-left", "17px");
            action = 2;
        } else {
            $("ul.subMenu li").css({"padding-left" : "45px", "text-align": "left", 'background': '#FBFBFD'});
            $("ul.subMenu li.roja").css({'background': '#FCF4F4','padding-left': '55px', 'color': '#F33C3B'});
            $("#contenedorMenu").css("width","20%");
            $("#contenedor").css("width","80%");  
            $("p.txt").fadeIn(2000);
            $("#menuAmb").fadeIn();
            $("#seller").fadeIn();
            $(".cajat1").css("margin-left", "10px");
            action = 1;
        }
    }  

    //Modulos Home
    $(function() {
  
        
        var items = $(".itemsModulos");
    
       
        items.click(function() {
            
            items.css('background', '#f4f7f9');
            
            $(this).css('background', '#fff');
        });
        

    });
    $("#ITMcontent2").click(function(){
        $("#content2").fadeIn();
        $("#content1").css("display","none");
        $("#content3").css("display","none");
        $("#content4").css("display","none");
        $("#content5").css("display","none");    
    });
    $("#ITMcontent1").click(function(){
        $("#content1").fadeIn();
        $("#content2").css("display","none");
        $("#content3").css("display","none");
        $("#content4").css("display","none");
        $("#content5").css("display","none");
    });
    $("#ITMcontent3").click(function(){
        $("#content3").fadeIn();
        $("#content2").css("display","none");
        $("#content1").css("display","none");
        $("#content4").css("display","none");
        $("#content5").css("display","none");
    });
    $("#ITMcontent4").click(function(){
        $("#content4").fadeIn();
        $("#content3").css("display","none");
        $("#content2").css("display","none");
        $("#content1").css("display","none");
        $("#content5").css("display","none");
    });
    $("#ITMcontent5").click(function(){
        $("#content5").fadeIn();
        $("#content3").css("display","none");
        $("#content2").css("display","none");
        $("#content1").css("display","none");
        $("#content4").css("display","none");
    });

    $(".imgInfo").hover(function(){
		$(".cajaInfo").toggle();
    });

    //pagina listaBanners
     
     $('#file_1').on('change',function(){
          $('#inputval').text( $(this).val());
      });

      $(".btnCerrar").click(function(){
        $("#modal1").fadeOut(); 
        $("#modal2").fadeOut(); 
        $("#modal3").fadeOut();
        $("#modal4").fadeOut(); 
        $("#modal5").fadeOut();
        $("#modal6").fadeOut(); 
        $("#modal7").fadeOut();
      });
     
      $("#btnEditar").click(function(){
        $("#subContenedorLista").css("display","none");
        $("#subContenedor2").fadeIn();
        $("#menuBanners").css("display","block"); 
        $("#menuBanners").animate({right: '0px'}, 1000);
        //$("html, body").animate({ scrollTop: 0 }, 600);
    });

    $("#subeIMG").click(function(){
        $("#modal4").fadeIn();
        $("#modal7").fadeIn(); 
    });
    $(".SubirIMG").click(function(){
        $("#modal4").fadeOut();
        $("#modal5").fadeIn(); 
    });
    $("#ACimagen").click(function(){
        $(".alertas").fadeIn();
        $("#modal5").fadeOut();
        $("#imagenBanner").fadeIn(); 
     });
    
     $("#GEguardar").click(function(){
        $(".alertas2").fadeIn(); 
        $("#menuBanners").animate({right: '-400px'}, 1000);
    }); 

    $(".lista1").click(function(){
        $("#select-box__input-text2").css("display","none");
        $("#select-box__input-text1").css("display","none");
    });
    $(".lista2").click(function(){
        $("#select-box__input-text2-2").css("display","none");
    });

    //pagina accesosDirectos
      $("#arw1").click(function(){
		$(".elimAD").slideToggle();
		$("#arw1").toggleClass("arwOnC");
		$("#ac").toggleClass("acOnC");
      });  
    
    $('#file_1').on('change',function(){
        $('#inputval').text( $(this).val());
    });
   
   $(".imgInfo").hover(function(){
       $(".cajaInfo").toggle();
    });
   
    $("button.editar.md1").click(function(){
        $("#modal1").fadeIn(); 
     });
    
     $("#elim").click(function(){
        $("#modal2").fadeIn(); 
     }); 

     $("#subir").click(function(){
        $("#modal2").fadeIn();
        $("#modal1").fadeOut(); 
     });

     $("#eliminar").click(function(){
        $(".alertas").fadeIn();
        $("#modal2").fadeOut();
     });
     $("#EditarTC").click(function(){
        $("#modal3").fadeIn(); 

     });

   $(".closeBtn1").click(function(){
       $(".alert").fadeOut();
    });
   $(".closeBtn").click(function(){
       $(".error").fadeOut();
    });
   
    //pagina productosTop
    $("#checkTodos").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
    $("#agregarPT").click(function(){
        $(".alertas").fadeIn();
     });

     $("#btnElimin").click(function(){
        $("#modal6").fadeIn();
     });
     //pagina Tiendas/Marcas
     $("#subirM7").click(function(){
        $(".alertas").fadeIn();
        $("#modal7").fadeOut();
        $("#imagenBanner").fadeIn(); 
     });
     //Emarsys
     $("#cambiar").click(function(){
        $(".alertas").fadeIn();
     });
});

//function menu() {
//    document.getElementById("canalesVenta").classList.toggle('show');
//    document.getElementById("imagen").classList.toggle('imagenroja');
//    }