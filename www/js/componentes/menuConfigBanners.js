Vue.component('menuconfigbanners',{
    template: //html
     `
    <div>
        <div id="menuBanners">
            <div class="actualizar">ACTUALIZAR</div>
            <div class="cajaMenuB" v-for="banerH of BanersHome">
            <div class="cajaMBmanita mb"><img class="inl-blk" src="img/index/arrow2.svg"/>
                <p class="inl-blk">{{banerH.estado}}</p>
            </div>
            <div class="cajaMBinfo">
                <p class="mb colorAz" id="subeIMG">Sube una imagen</p>
                <p class="mb">Url: {{banerH.url}}</p>
                <p class="mb">Vigencia: {{banerH.vigencia}}</p>
                <p class="mb inl-blk mr">Estatus: </p>
                <div class="card inl-blk">
                <div class="material-toggle">
                    <input class="switch" type="checkbox" :id="banerH.toggle" :name="banerH.toggle" checked="true"/>
                    <label :for="banerH.toggle"></label>
                </div>
                </div>
                <p class="mb colorAz">Eliminar           </p>
            </div>
            </div>
            <button class="btnAgregar">Agregar</button>
            <button class="GEApp" id="GEguardar">Guardar</button>
        </div>
</div>
    `,
    data(){
        return{
            BanersHome: [
                {estado:3, url:'/I/hotsale.com', vigencia: '01-01-19 a 03-05-19',toggle: 'toggle1' },
                {estado:2, url:'/I/hotsale.com', vigencia: '01-01-19 a 03-05-19',toggle: 'toggle2' },
                {estado:3, url:'/I/hotsale.com', vigencia: '01-01-19 a 03-05-19', toggle: 'toggle3' }
            ]
        }
    }
});