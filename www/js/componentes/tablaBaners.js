Vue.component('tablabaners',{
    template: //html
    `
    <div>
        <div class="contenedorTabla">
            <table id="tabla1">
            <thead class="stick">
                <tr>
                <th class="stick" v-for="titulo of titulos">{{titulo}}</th>
                </tr>
            </thead>
            <tbody id="cuerpoT">
                <tr v-for="baner of Baners">
                <td class="txtCenter">{{baner.posicion}}</td>
                <td>
                    <div class="inl-blk" v-if="baner.estatus==='Activo'">
                    <div class="estatus estatus1"></div>
                    </div>
                    <div class="inl-blk" v-else-if="baner.estatus==='Vencido'">
                    <div class="estatus estatus2"></div>
                    </div>
                    <div class="inl-blk" v-else="v-else">
                    <div class="estatus estatus3"></div>
                    </div>{{baner.estatus}}
                </td>
                <td>{{baner.nombre}}</td>
                <td><img class="imgTabla" src="img/index/celular-img.svg"/></td>
                <td>{{baner.pagina}}</td>
                <td>{{baner.fechahora}}</td>
                </tr>
            </tbody>
            </table>
        </div>
</div>
    `,
    data(){
        return{
            titulos: ['Posición','Estatus','Nombre','Publicidad','Página','Fecha/Hora'],
            Baners : [
                {posicion: 1,estatus: 'Activo', nombre: 'Regreso a clases', pagina: 'Landing', fechahora: '11/08/19-12:00' },
                {posicion: 2,estatus: 'Vencido', nombre: 'Hotsale', pagina: 'Landing', fechahora: '11/08/19-12:00' },
                {posicion: 3,estatus: '1 día: 30min', nombre: 'Celulares y Tecnología', pagina: 'Categoría', fechahora: '11/08/19-12:00' },
                {posicion: 4,estatus: 'Activo', nombre: 'Samsumg', pagina: 'Producto', fechahora: '11/08/19-12:00' },
                {posicion: 5,estatus: 'Vencido', nombre: 'PrimaveraVerano', pagina: 'Corner', fechahora: '11/08/19-12:00' },
                {posicion: 6,estatus: '1 día: 30min', nombre: 'Regreso a clases', pagina: 'Landing', fechahora: '11/08/19-12:00' }
            ]
        }
    }
});